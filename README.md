# webapp-template
a minimal template for creating a react webapp, featuring:
- react
- redux
- styled-components
- esbuild
- bun
- hono
- sqlite

along with some functional augments like:
- fp-ts
- ts-pattern

and a nix flake for easy development and deployment

# installation
clone in the repo and run nix:
```
  git clone https://gitlab.com/odd/webapp-template
  cd webapp-template
  nix develop
```
