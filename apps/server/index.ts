import { Hono } from 'hono'
import { getAll, persistValue } from '../db/client'

const app = new Hono()
const meta = {
  port: 8001,
  fetch: app.fetch,
}

console.log(`starting up server on http://localhost:${meta.port}!`)

app.get('/', c => c.text('Hono!!'))
app.get('/data', c => c.json(getAll()))

app.post('/data/:clientid/:value', c => {
  const { clientid, value } = c.req.param()
  persistValue(parseInt(clientid), parseInt(value))
  return c.json({ clientid, value })
})

export default meta
