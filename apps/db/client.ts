import { BunSQLiteDatabase, drizzle } from 'drizzle-orm/bun-sqlite'
import { Database } from 'bun:sqlite'
import { dataPoints } from './schema'

// in practice, this would be a secrets managed url
const sqlite = new Database('data.db')
export const db = drizzle(sqlite)

const getAllDb = (db: BunSQLiteDatabase) => () =>
  db.select().from(dataPoints).all()

const persistValueDb = (db: BunSQLiteDatabase) => (clientId, value) => {
  db.insert(dataPoints).values({ clientId, value })
}

export const [getAll, persistValue] = [getAllDb(db), persistValueDb(db)]
