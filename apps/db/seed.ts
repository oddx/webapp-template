import { db } from "./client";
import { dataPoints } from "./schema";

db.insert(dataPoints).values([
  {
    clientId: 1,
    value: .5,
  },
  {
    clientId: 2,
    value: .15,
  },
  {
    clientId: 3,
    value: .75,
  }
])

console.log('Seeding complete :D')
