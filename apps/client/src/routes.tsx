import { lazy } from 'react'
import { Navigate, Outlet, createBrowserRouter } from 'react-router-dom'
import { useAppContext } from './AppContext'

const AuthedApp = lazy(() => import('./AuthedApp'))
const UnauthedApp = lazy(() => import('./UnauthedApp'))

const PrivateRoutes = () => {
  const { user } = useAppContext()
  return user ? <Outlet context={user} /> : <Navigate to='/login' />
}

const About = () => <h1>About</h1>
const Error = () => <h1>Error :(</h1>
const NotFound = () => <h1>404 :(</h1>


export const router = createBrowserRouter([
  {
    path: '*',
    element: <NotFound />,

  },
  {
    path: '/',
    element: <PrivateRoutes />,
    errorElement: <Error />,
    children: [
      {
        path: '/',
        element: <AuthedApp />,
      },
      {
        path: 'about',
        element: <About />
      }
    ]
  },
  {
    path: 'login',
    element: <UnauthedApp />
  },
])


