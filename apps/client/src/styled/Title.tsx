import React from 'react'
import styled from 'styled-components'


const StyledTitle = styled.h1`
  text-align: center;
`

const Title = ({children}) =>
  <StyledTitle>
    {children}
  </StyledTitle>

export default Title;
