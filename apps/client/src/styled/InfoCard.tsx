import React from 'react'
import styled from 'styled-components'

const StyledInfoContainer = styled.div`
  boder-radius: 1rem;
  background: #eee;
  box-shadow: 0 8px 8px -4px lightblue;
`

const StyledInfoHeader = styled.h2`
  text-align: center;
`

const StyledInfoBody = styled.ul`
  list-style-type: space-counter;
`

const StyledInfoListItem = styled.li`
  
`

const InfoCard = ({title, elements}) =>
  <StyledInfoContainer>
    <StyledInfoHeader>{title}</StyledInfoHeader>
    <StyledInfoBody>
      {elements.map(x => <StyledInfoListItem key={x}>{x}</StyledInfoListItem>)}
    </StyledInfoBody>
  </StyledInfoContainer>

export default InfoCard;
