import { createRoot } from 'react-dom/client'
import { Provider } from 'react-redux'

import { store } from './store/store'
import { AppProvider } from './AppContext'
import { RouterProvider } from 'react-router-dom'
import { router } from './routes'
import { StrictMode } from 'react'

createRoot(document.getElementById('root')!)
  .render(
    <StrictMode>
      <Provider store={store}>
        <AppProvider>
          <RouterProvider router={router} />
        </AppProvider>
      </Provider>
    </StrictMode>
  )
