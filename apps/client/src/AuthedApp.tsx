import { useDispatch, useSelector } from 'react-redux'
import Title from './styled/Title'
import InfoCard from './styled/InfoCard'
import type { RootState } from './store/store'
import { increment } from './store/slices/counter'
import { getAll, handleGet } from './api/client'
import { useOutletContext } from 'react-router'
import { Link } from 'react-router-dom'

const AuthedApp = () => {
  const count = useSelector((state: RootState) => state.counter.value)
  const dispatch = useDispatch()
  const user = useOutletContext<{ name: string, locations: string[] }>()

  const handleClick = () => dispatch(increment())

  return (
    <>
      <Title>{user.name}</Title>
      <InfoCard title="locations" elements={user.locations} />
      <button onClick={handleClick}>{count}</button>
      <div
        onClick={async () => handleGet(await getAll())}
      >
        click me for data (broken while drizzle borked)
      </div>
      <Link to='about'>about</Link>
    </>
  )
}

export default AuthedApp
