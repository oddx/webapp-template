import React, { createContext, useContext, useEffect, useState } from "react"

interface AppContextType {
  user: object
}

const AppContext = createContext({})

const userData = {
  name: "me",
  settings: { isAwesome: true },
  locations: [
    "Tucson, Arizona",
    "Hoh Chi Min, Vietnam",
    "Bangkok, Thailand"
  ]
}

const login = () => ({
  // create cookie 
})

const logout = () => ({
  // delete cookie
})

const sleep = (time) =>  new Promise(resolve => setTimeout(resolve, time))
const getUser = () => sleep(1000).then(() => (userData))

export const AppProvider = ({children}) => {
  const [state, setState] = useState({
    status: 'pending',
    error: null,
    user: {},
  })
  
  useEffect(() => {
    getUser().then(
    user => setState({status: 'success', user: user, error: null}),
    error => setState({status: 'error', error: error, user: {}}))
  }, [])

  return (
    <AppContext.Provider value={ state }>
      { state.status === 'pending' ? <h1>Loading...</h1>: children}
    </AppContext.Provider>
  )
}

export const useAppContext = () => useContext(AppContext) as AppContextType